"""projekt URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('erledigt', views.erledigt, name='erledigt'),
    path('sicherheitsabfrage-alle', views.sicherheitsabfrage_alle, name='sicherheitsabfrage_alle'),
    path('sicherheitsabfrage/<int:todo_id>', views.sicherheitsabfrage, name='sicherheitsabfrage'),
    path('alle-loeschen', views.alle_loeschen, name='alle_loeschen'),
    path('loeschen/<int:todo_id>', views.loeschen, name='loeschen'),
    path('neu', views.neu, name='neu'),
    path('todo-erledigen/<int:todo_id>', views.todo_erledigen, name='todo_erledigen'),
    path('todo-offen/<int:todo_id>', views.todo_offen, name='todo_offen'),
]

