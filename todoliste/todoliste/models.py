from django.db import models

# Create your models here.

class Todo(models.Model):
    # id macht Django automatisch
    name = models.CharField(max_length=200)
    erledigt = models.BooleanField(default=False)

    def erledigen(self):
        '''
        Eine kleine Demonstration, wie man Methoden definiert.
        Eine Methode ist eine Funktion, die auf einer konkreten Instanz einer Todo arbeitet.
        Die Instanz wird dabei über "self" angesprochen.
        '''
        self.erledigt = True

    def __str__(self):
        erledigt_text = "erledigt"
        if not self.erledigt:
            erledigt_text = "offen"
        return f"{self.name} ({erledigt_text})"
        # return "%s (%s)" % [self.name, erledigt_text]
        # return "{} ({})".format(self.name, erledigt_text)
        # return self.name + " (" + erledigt_text + ")"