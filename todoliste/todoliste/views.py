from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.

from . import models

def index(request):
    context = {}
    # Beispiel mit einfacher Liste von Dictionaries
    #context["todos"] = [
    #    {"name": "bla", "erledigt": False},
    #    {"name": "blubb", "erledigt": True},
    #    ]
    context["todos"] = models.Todo.objects.filter(erledigt=False)
    return render(request, "todoliste/index.html", context)

def erledigt(request):
    context = {}
    context["todos"] = models.Todo.objects.filter(erledigt=True)
    return render(request, "todoliste/erledigt.html", context)

def sicherheitsabfrage_alle(request):
    return render(request, "todoliste/sicherheitsabfrage_alle.html")

def sicherheitsabfrage(request, todo_id):
    context = {}
    context["todo"] = get_object_or_404(models.Todo, pk=todo_id)
    return render(request, "todoliste/sicherheitsabfrage.html", context)

def alle_loeschen(request):
    models.Todo.objects.filter(erledigt=True).delete()
    return redirect("index")

def loeschen(request, todo_id):
    todo = get_object_or_404(models.Todo, pk=todo_id)
    todo.delete()
    return redirect("erledigt")


def neu(request):
    if request.POST:
        neue_todo = models.Todo()
        neue_todo.name = request.POST["name"]
        neue_todo.save()
    return redirect("index")


def todo_erledigen(request, todo_id):
    todo = get_object_or_404(models.Todo, pk=todo_id)
    todo.erledigt = True
    todo.save()
    return redirect("index")


def todo_offen(request, todo_id):
    todo = get_object_or_404(models.Todo, pk=todo_id)
    todo.erledigt = False
    todo.save()
    return redirect("erledigt")